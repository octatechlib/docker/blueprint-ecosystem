# Blueprint Ecosystem

General ecosystem for anything that is related to the project. Separated services up and running as one.

## Getting started

This is a blueprint for basic ecosystem docker based configuration. All separated services configurated and connected running as combined system.

#### Must know
Meant for MacOS. Other ymls soon.

## License
[![License](https://img.shields.io/badge/License-GPL_2.0-blue.svg)](https://opensource.org/license/gpl-2-0/)

## Project status
In progress.