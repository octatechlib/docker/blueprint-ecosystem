include .env.development

USER_UID := $(shell id -u)
USER_NAME := $(shell id -un)
USER_GID := $(shell id -g)
USER_GROUP := $(shell id -g -n)

##---------------------------------------------------------------------------- help

help:##
	@echo ""
	@echo "Commands:"
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'
	@echo ""
.PHONY: help

ONESHELL:
docker-print-variables:   ## Show variables
	@echo ""
	@echo "Ecosystem"
	@echo ""
	@echo "      frontend http://$(ECO_IP_FRONTEND_SERVICE) email: $(ECO_USER_EMAIL) password: $(ECO_USER_PASSWORD)"
	@echo "          auth http://$(ECO_IP_AUTH_SERVICE)"
	@echo "           api http://$(ECO_IP_API_SERVICE)"
	@echo "       storage http://$(ECO_IP_STORAGE_SERVICE)"
	@echo " db management http://$(ECO_IP_DB_MANAGEMENT) username: $(ECO_DATABASE_MAIN_USER) password: $(ECO_DATABASE_MAIN_USER_PASSWORD)"
	@echo "       mailhog http://$(ECO_IP_MAILHOG):8025"
	@echo ""
.PHONY: docker-print-variables

.ONESHELL:
docker-start:             ## Start docker containers for ecosystem development
	@echo "Starting services"
	@docker compose --env-file=.env.development --profile ecosystem_development start
	@make -s docker-print-variables
.PHONY: docker-start

.ONESHELL:
docker-stop:              ## Stop docker containers for ecosystem development
	@echo "Stoping services"
	@docker compose --env-file=.env.development --profile ecosystem_development stop
.PHONY: docker-stop

.ONESHELL:
docker-up:                ## Go up with all docker containers for ecosystem development
	@if [ ! -d "app-auth" ]; then echo "app-auth is not installed"; exit 0; fi
	@if [ ! -d "app-api" ]; then echo "app-api is not installed"; exit 0; fi
	@if [ ! -d "app-storage" ]; then echo "app-storage is not installed"; exit 0; fi
	@if [ ! -d "app-frontend" ]; then echo "app-frontend is not installed"; exit 0; fi
	@
	@echo "All services going UP"
	@USER_NAME=$(USER_NAME) USER_UID=$(USER_UID) USER_GROUP=$(USER_GROUP) USER_GID=$(USER_GID) docker compose --env-file=.env.development --profile=ecosystem_development up -d
	@echo "all services are UP"
	@make -s docker-print-variables
.PHONY: docker-up

.ONESHELL:
docker-down:              ## Go down with all docker containers for ecosystem development
	@echo "Services going DOWN"
	@docker compose --env-file=.env.development --profile ecosystem_development down
.PHONY: docker-down

.ONESHELL:
docker-install-backend:   ## Build all services for ecosystem development backend (auth, api, storage) TODO: load repos
	@if [ ! -d "app-auth" ]; then echo "Will clone auth app" && GIT_SSH_COMMAND="ssh -i ${SSH_CERT_PATH}" git clone ${REPO_AUTH} ${PWD}/app-auth; fi
	@if [ ! -d "app-api" ]; then echo "Will clone api app" && GIT_SSH_COMMAND="ssh -i ${SSH_CERT_PATH}" git clone ${REPO_API} ${PWD}/app-api; fi
	@if [ ! -d "app-storage" ]; then echo "Will clone storage app" && GIT_SSH_COMMAND="ssh -i ${SSH_CERT_PATH}" git clone ${REPO_STORAGE} ${PWD}/app-storage; fi
	@
	@USER_UID=$(USER_UID) USER_GID=$(USER_GID) docker compose --env-file=.env.development --profile=ecosystem_backend up --build -d
	@
	@if [ -f "./app-auth/.env" ]; then rm ./app-auth/.env; fi
	@touch ./app-auth/.env
	@cat core/.env.base.auth >  ./app-auth/.env
	@
	@echo "ECO_AUTH_SERVICE_NAME=\"$(ECO_AUTH_SERVICE_NAME)\"" | tee -a ./app-auth/.env
	@echo "ECO_IP_AUTH_SERVICE=\"$(ECO_IP_AUTH_SERVICE)\"" | tee -a ./app-auth/.env
	@echo "ECO_IP_FRONTEND_SERVICE=\"$(ECO_IP_FRONTEND_SERVICE)\"" | tee -a ./app-auth/.env
	@echo "ECO_IP_API_SERVICE=\"$(ECO_IP_API_SERVICE)\"" | tee -a ./app-auth/.env
	@echo "ECO_IP_STORAGE_SERVICE=\"$(ECO_IP_STORAGE_SERVICE)\"" | tee -a ./app-auth/.env
	@echo "" | tee -a ./app-auth/.env
	@echo "ECO_IP_DATABASE_MAIN=\"$(ECO_IP_DATABASE_MAIN)\"" | tee -a ./app-auth/.env
	@echo "ECO_DATABASE_MAIN=\"$(ECO_DATABASE_MAIN)\"" | tee -a ./app-auth/.env
	@echo "ECO_DATABASE_MAIN_USER=\"$(ECO_DATABASE_MAIN_USER)\"" | tee -a ./app-auth/.env
	@echo "ECO_DATABASE_MAIN_USER_PASSWORD=\"$(ECO_DATABASE_MAIN_USER_PASSWORD)\"" | tee -a ./app-auth/.env
	@echo "" | tee -a ./app-auth/.env
	@echo "ECO_IP_MAILHOG=\"$(ECO_IP_MAILHOG)\"" | tee -a ./app-auth/.env
	@echo "" | tee -a ./app-auth/.env
	@
	@if [ -f "./app-api/.env" ]; then rm ./app-api/.env; fi
	@touch ./app-api/.env
	@cat core/.env.base.api >  ./app-api/.env
	@
	@echo "ECO_API_SERVICE_NAME=\"$(ECO_API_SERVICE_NAME)\"" | tee -a ./app-api/.env
	@echo "ECO_IP_AUTH_SERVICE=\"$(ECO_IP_AUTH_SERVICE)\"" | tee -a ./app-api/.env
	@echo "ECO_IP_FRONTEND_SERVICE=\"$(ECO_IP_FRONTEND_SERVICE)\"" | tee -a ./app-api/.env
	@echo "ECO_IP_API_SERVICE=\"$(ECO_IP_API_SERVICE)\"" | tee -a ./app-api/.env
	@echo "ECO_IP_STORAGE_SERVICE=\"$(ECO_IP_STORAGE_SERVICE)\"" | tee -a ./app-api/.env
	@echo "" | tee -a ./app-api/.env
	@echo "ECO_IP_DATABASE_MAIN=\"$(ECO_IP_DATABASE_MAIN)\"" | tee -a ./app-api/.env
	@echo "ECO_DATABASE_MAIN=\"$(ECO_DATABASE_MAIN)\"" | tee -a ./app-api/.env
	@echo "ECO_DATABASE_MAIN_USER=\"$(ECO_DATABASE_MAIN_USER)\"" | tee -a ./app-api/.env
	@echo "ECO_DATABASE_MAIN_USER_PASSWORD=\"$(ECO_DATABASE_MAIN_USER_PASSWORD)\"" | tee -a ./app-api/.env
	@echo "" | tee -a ./app-api/.env
	@echo "ECO_IP_MAILHOG=\"$(ECO_IP_MAILHOG)\"" | tee -a ./app-api/.env
	@echo "" | tee -a ./app-api/.env
	@
	@if [ -f "./app-storage/.env" ]; then rm ./app-storage/.env; fi
	@touch ./app-storage/.env
	@cat core/.env.base.storage >  ./app-storage/.env
	@
	@echo "ECO_STORAGE_SERVICE_NAME=\"$(ECO_API_SERVICE_NAME)\"" | tee -a ./app-storage/.env
	@echo "ECO_IP_AUTH_SERVICE=\"$(ECO_IP_AUTH_SERVICE)\"" | tee -a ./app-storage/.env
	@echo "ECO_IP_FRONTEND_SERVICE=\"$(ECO_IP_FRONTEND_SERVICE)\"" | tee -a ./app-storage/.env
	@echo "ECO_IP_API_SERVICE=\"$(ECO_IP_API_SERVICE)\"" | tee -a ./app-storage/.env
	@echo "ECO_IP_STORAGE_SERVICE=\"$(ECO_IP_STORAGE_SERVICE)\"" | tee -a ./app-storage/.env
	@echo "" | tee -a ./app-storage/.env
	@echo "ECO_IP_DATABASE_MAIN=\"$(ECO_IP_DATABASE_MAIN)\"" | tee -a ./app-storage/.env
	@echo "ECO_DATABASE_MAIN=\"$(ECO_DATABASE_MAIN)\"" | tee -a ./app-storage/.env
	@echo "ECO_DATABASE_MAIN_USER=\"$(ECO_DATABASE_MAIN_USER)\"" | tee -a ./app-storage/.env
	@echo "ECO_DATABASE_MAIN_USER_PASSWORD=\"$(ECO_DATABASE_MAIN_USER_PASSWORD)\"" | tee -a ./app-storage/.env
	@echo "" | tee -a ./app-storage/.env
	@echo "ECO_IP_MAILHOG=\"$(ECO_IP_MAILHOG)\"" | tee -a ./app-storage/.env
	@echo "" | tee -a ./app-storage/.env
	@
	@USER_UID=$(USER_UID) USER_GID=$(USER_GID) docker compose --env-file=.env.development exec ecosystem_auth_service composer install
	@USER_UID=$(USER_UID) USER_GID=$(USER_GID) docker compose --env-file=.env.development exec ecosystem_auth_service composer post-install-cmd
	@
	@USER_UID=$(USER_UID) USER_GID=$(USER_GID) docker compose --env-file=.env.development exec ecosystem_api_service composer install
	@USER_UID=$(USER_UID) USER_GID=$(USER_GID) docker compose --env-file=.env.development exec ecosystem_api_service composer post-install-cmd
	@
	@USER_UID=$(USER_UID) USER_GID=$(USER_GID) docker compose --env-file=.env.development exec ecosystem_storage_service composer install
	@USER_UID=$(USER_UID) USER_GID=$(USER_GID) docker compose --env-file=.env.development exec ecosystem_storage_service composer post-install-cmd
	@
	@
	@make -s docker-print-variables
.PHONY: docker-install-backend

.ONESHELL:
docker-install-frontend:  ## Build frontend services for ecosystem development frontend
	@if [ ! -d "app-frontend" ]; then echo "Will clone frontend app" && GIT_SSH_COMMAND="ssh -i ${SSH_CERT_PATH}" git clone ${REPO_FRONTEND} ${PWD}/app-api; fi
	@USER_UID=$(USER_UID) USER_GID=$(USER_GID) docker compose --env-file=.env.development up ecosystem_frontend --build
	@
	@if [ -f "./app-frontend/.env" ]; then rm ./app-frontend/.env; fi
	@touch ./app-frontend/.env
	@cat core/.env.base.frontend >  ./app-frontend/.env
	@
	@echo "ECO_AUTH_SERVICE_NAME=\"$(ECO_AUTH_SERVICE_NAME)\"" | tee -a ./app-frontend/.env
	@echo "ECO_IP_AUTH_SERVICE=\"$(ECO_IP_AUTH_SERVICE)\"" | tee -a ./app-frontend/.env
	@echo "ECO_IP_FRONTEND_SERVICE=\"$(ECO_IP_FRONTEND_SERVICE)\"" | tee -a ./app-frontend/.env
	@echo "" | tee -a ./app-frontend/.env
	@echo "ECO_API_SERVICE_NAME=\"$(ECO_API_SERVICE_NAME)\"" | tee -a ./app-frontend/.env
	@echo "ECO_IP_API_SERVICE=\"$(ECO_IP_API_SERVICE)\"" | tee -a ./app-frontend/.env
	@echo "" | tee -a ./app-frontend/.env
	@echo "ECO_IP_DATABASE_MAIN=\"$(ECO_IP_DATABASE_MAIN)\"" | tee -a ./app-frontend/.env
	@echo "ECO_DATABASE_MAIN=\"$(ECO_DATABASE_MAIN)\"" | tee -a ./app-frontend/.env
	@echo "ECO_DATABASE_MAIN_USER=\"$(ECO_DATABASE_MAIN_USER)\"" | tee -a ./app-frontend/.env
	@echo "ECO_DATABASE_MAIN_USER_PASSWORD=\"$(ECO_DATABASE_MAIN_USER_PASSWORD)\"" | tee -a ./app-frontend/.env
	@echo "" | tee -a ./app-frontend/.env
	@echo "ECO_IP_MAILHOG=\"$(ECO_IP_MAILHOG)\"" | tee -a ./app-frontend/.env
	@echo "ECO_AUTH_SERVICE_NAME=\"$(ECO_AUTH_SERVICE_NAME)\"" | tee -a ./app-frontend/.env
	@echo "" | tee -a ./app-frontend/.env
	@
.PHONY: docker-install-frontend

.ONESHELL:
docker-logs:              ## Show logs
	@echo "Docker LOGS"
	@USER_UID=$(USER_UID) USER_GID=$(USER_GID) docker compose --env-file=.env.development logs
.PHONY: docker-logs

##
##-------------------------------------------------------------------- auth service

.ONESHELL:
docker-logs-auth_service:    ## Show logs for auth service
	@echo "Docker LOGS"
	@USER_UID=$(USER_UID) USER_GID=$(USER_GID) docker compose --env-file=.env.development logs ecosystem_auth_service --follow
.PHONY: docker-logs-auth_service

##--------------------------------------------------------------------- api service

.ONESHELL:
docker-logs-api_service:     ## Show logs for api service
	@echo "Docker LOGS"
	@USER_UID=$(USER_UID) USER_GID=$(USER_GID) docker compose --env-file=.env.development logs ecosystem_api_service --follow
.PHONY: docker-logs-api_service

##----------------------------------------------------------------- storage service

.ONESHELL:
docker-logs-storage_service: ## Show logs for storage service
	@echo "Docker LOGS"
	@USER_UID=$(USER_UID) USER_GID=$(USER_GID) docker compose --env-file=.env.development logs ecosystem_api_service --follow
.PHONY: docker-logs-api_service


